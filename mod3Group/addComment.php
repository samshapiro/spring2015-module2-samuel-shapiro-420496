!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>

<link  href ="bootstrap.min.css" rel= "stylesheet">
<link href="starter.css" rel="stylesheet">

</head>
<body>
<?php
    session_start();
    $mysqli = new mysqli('172.31.42.5', 'wustl_inst', 'wustl_pass', 'mod3_g'); 
      if($mysqli->connect_errno) {
              printf("Connection Failed: %s\n", $mysqli->connect_error);
              echo "dooo";
              exit;
      }
    $id = htmlentities($_POST['story_id']);
?>
 <!-- Navigation -->
     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="homepage.php">SW  News</a>
            </div>
            <div class="pull-right">
            </div>
            <!--/.navbar-collapse -->
            <!-- Collect the nav links, forms, and other content for toggling -->
        </div>
        <!-- /.container -->
    </nav>
     
     <div class="container">

        <div class="row">
            <div class="col-lg-8">
                <h1 class="page-header">Comment
                    <small> make your comment here</small>
                </h1>
            </div>

        </div>
        
        <div class="row">
            <div>

            <?php
            $newPost = '
                <form class="col-lg-8 new-post" role="form" action="post_comment.php" method="post">
                <div class="form-group">
                    <textarea class="col-lg-8 form-control text-box" rows="10" name="body"></textarea>
                </div>
                <input type="hidden" name="story_id" value="'.$id.'">
                <div class="form-group">
                    <button type="submit" class="btn btn-success post-button">Post</button>
                </div>
                </form>';

           
           echo $newPost;
            ?>
            
            </div>
        </div>
</body>

</html>