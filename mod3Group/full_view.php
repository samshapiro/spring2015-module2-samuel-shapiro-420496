!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>

<link  href ="bootstrap.min.css" rel= "stylesheet">
<link href="starter.css" rel="stylesheet">

</head>
<body>
<?php
    session_start();
    $mysqli = new mysqli('172.31.42.5', 'wustl_inst', 'wustl_pass', 'mod3_g'); 
      if($mysqli->connect_errno) {
              printf("Connection Failed: %s\n", $mysqli->connect_error);
              echo "dooo";
              exit;
      }
    $id = htmlentities($_POST['story_id']);
    $safe=$id;
    
?>
<!-- Navigation -->
     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="homepage.php">SW  News</a>
            </div>
            <div class="pull-right">
                
                <?php
                $guest = '';

                $user = '
                <form class="navbar-form navbar-left" role="form" action="addComment.php" method="post" style="display:inline-block">
                <input type="hidden" name="story_id" value="'.$id.'">
                <button type="submit" class="btn btn-primary" name="action" value="new_post">New Comment</button>';
                
                

                    if (isset($_SESSION['user_id'])) {
                       echo $user;
                    } else {
                        echo $guest;
                    }
                ?>
            </div>
            <!--/.navbar-collapse -->
            <!-- Collect the nav links, forms, and other content for toggling -->
        </div>
        <!-- /.container -->
    </nav>
        
     <div class="container">
        <?php
        echo '<p></p>';
       $stmt = $mysqli->prepare("select subject, text, created, user_id from stories where subject = '$id' ");
                    if ( !$stmt) {
                        printf("Query Prep Failed: %s\n", $mysqli->error);
                        error;
                    }

                    $stmt->execute();
                    $stmt->bind_result($subject, $body, $created, $id);
                    echo '<ul style="list-style:none">';
                    while($stmt->fetch()) {
                        echo '
                        <li>
                            <div class="row">
                                <div class="col-md-8 portfolio-item main-news">
                                    <h3>
                                        <a>'. $subject .'</a>
                                    </h3>
                                    <p>'. $body.'...</p>
                                    <h6>Posted on:'. $created .'</h6>
                                    </form>
                          </div>
                            </div>
                        </li>
                        ';
                    }
                    echo '</ul>';
        ?>
     
    </div>
       <div class="jumbotron secondary min-top-padding ">
                <h4 style="text-align: center">Comments</h4>
        </div>
       
      <?php
       $stmt2 = $mysqli->prepare("select story, user, content, created from comments where story='$subject' order by created desc ");
                    if ( !$stmt2) {
                        printf("Query Prep Failed: %s\n", $mysqli->error);
                        error;
                    }
                    $stmt2->execute();
                    $stmt2->bind_result($story, $user, $text, $created);
                    echo '<ul style="list-style:none">';
                    while($stmt2->fetch()) {
                        echo '
                        <li>
                            <div class="row">
                                <div class="col-md-8 portfolio-item main-news">
                                    <h3>
                                        <a>'. $user .'</a>
                                    </h3>
                                    <p>'.$text.'...</p>
                                    <h6>Posted on:'. $created .'</h6>
                        ';
                        
                        if ($_SESSION['user_id'] == $user){
                            echo '
                            <form action="removeCom.php" method="POST">
                                    <input type="hidden" name="story_id" value="'.$text.'">
                                    <button type="submit" class="btn btn-danger" name="action" value="remove">Delete</button>
                            </form>
                             <form action="editCom.php" method="POST">
                                     <input type="hidden" name="story_id" value="'.$text.'">
                                      <input type="hidden" name="actual_id" value="'.$story.'">
                                    <button type="submit" class="btn btn-warning" name="action" value="edit">Edit</button>
                            </form>
                            ';
                        }
                        echo '
                          </div>
                            </div>
                        </li>
                        ';
                    }
                    echo '</ul>';
        ?>
     
     
     
     
</body>

</html>