<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>

<link  href ="bootstrap.min.css" rel= "stylesheet">
<link href="starter.css" rel="stylesheet">

</head>
<body>
<?php
    session_start();
    $mysqli = new mysqli('172.31.42.5', 'wustl_inst', 'wustl_pass', 'mod3_g'); 
      if($mysqli->connect_errno) {
              printf("Connection Failed: %s\n", $mysqli->connect_error);
              echo "dooo";
              exit;
      }
?>
 <!-- Navigation -->
     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="homepage.php">SW  News</a>
            </div>
            <div class="pull-right">
                
                <?php
                $guest = '
                <form class="navbar-form navbar-left" role="form" action="login.php" method="post">
                <div class="form-group">
                  <input type="text" placeholder="Username" class="form-control" name="user" id="emailInput">
                </div>
                <div class="form-group">
                  <input type="password" placeholder="Password" class="form-control" name="pass" id="passInput">
                </div>
                <button type="submit" class="btn btn-warning" name="action" value="Sign">Sign in</button>
                
                <button type="submit" class="btn btn-primary" name="action" value="Register">Register</button>
                </form>';

                $user = '
                <form class="navbar-form navbar-left" role="form" action="logout.php" method="post" style="display:inline-block">
                <div class="form-group">
                  <input type="text" placeholder="Have" class="form-control" name="user" id="emailInput" disabled="disabled">
                </div>
                <div class="form-group">
                  <input type="password" placeholder="Fun!" class="form-control" name="pass" id="passInput" disabled="disabled">
                </div>
                <button type="submit" class="btn btn-primary" name="action" value="new_post">New Post</button>
                
                <button type="submit" class="btn btn-warning" name="action" value="logout">Sign Out</button>';
                
                

                    if (isset($_SESSION['user_id'])) {
                       echo $user;
                    } else {
                        echo $guest;
                    }
                ?>
            </div>
            <!--/.navbar-collapse -->
            <!-- Collect the nav links, forms, and other content for toggling -->
        </div>
        <!-- /.container -->
    </nav>
    
    <div class="container">

        <!-- Page Header -->
        <div class="row">
            <p></p>
               <p></p>
               <p></p>
               <p></p>
            <div class="col-lg-8">
                <h1 class="page-header">SW News
                    <small>All the big stories are here</small>
                </h1>
            </div>
            <div class="col-lg-4 post">
            <?php
                $user='
                <form role="form" action="new_post.php" method="post">
                    <button type="submit" class="btn btn-primary" name="action" value="new_post">New Post</button>
                </form>';
                if (isset($_SESSION['user_id'])){
                    echo $user;
                }
            ?>
            </div>
        </div>
        <!-- /.row -->
    
        <!-- Projects Row -->
        <?php
       $stmt = $mysqli->prepare("select subject, text, created, user_id from stories order by created desc ");
                    if ( !$stmt) {
                        printf("Query Prep Failed: %s\n", $mysqli->error);
                        error;
                    }

                    $stmt->execute();
                    $stmt->bind_result($subject, $body, $created, $id);
                    echo '<ul style="list-style:none">';
                    while($stmt->fetch()) {
                        echo '
                        <li>
                            <div class="row">
                                <div class="col-md-8 portfolio-item main-news">
                                    <h3>
                                        <a>'. $subject .'</a>
                                    </h3>
                                    <p>'. $body.'...</p>
                                    <h6>Posted on:'. $created .'</h6>
                                    <form action="full_view.php" method="POST">
                                        <input type="hidden" name="story_id" value="'.$subject.'">
                                        <button type="submit" name="action" value="full_view">Read More</button>
                                    </form>
                        ';
                        
                        if ($_SESSION['user_id'] == $id){
                            echo '
                            <form action="remove.php" method="POST">
                                    <input type="hidden" name="story_id" value="'.$subject.'">
                                    <button type="submit" class="btn btn-danger" name="action" value="remove">Delete</button>
                            </form>
                             <form action="edit.php" method="POST">
                                    <input type="hidden" name="story_id" value="'.$subject.'">
                                    <button type="submit" class="btn btn-warning" name="action" value="edit">Edit</button>
                            </form>
                            ';
                        }
                        echo '
                          </div>
                            </div>
                        </li>
                        ';
                    }
                    echo '</ul>';
        ?>
    <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; SWNews</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

</body>
</html>