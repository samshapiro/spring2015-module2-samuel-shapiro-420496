    	<?php

            ini_set('display_errors', 'On');
            session_start();
            $mysqli = new mysqli('172.31.42.5', 'wustl_inst', 'wustl_pass', 'mod3_g'); 
            if(!isset($_SESSION['user_id'])){
                header("Location: mainpage.php");
            }

    		$subject = htmlentities($_POST['subject']);
    		$body = htmlentities($_POST['body']);
                $id = htmlentities($_SESSION['user_id']);

            
            $stmt = $mysqli->prepare("INSERT INTO stories (user_id, subject, text) VALUES (?, ?, ?)");
                if(!$stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
            $stmt->bind_param('sss', $id, $subject, $body);
            $stmt->execute();
            $stmt->close();


            header("Location: homepage.php");
    	?>

    </div>

</body>

</html>