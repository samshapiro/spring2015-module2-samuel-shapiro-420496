    	<?php
            ini_set('display_errors', 'On');
            session_start();
            $mysqli = new mysqli('172.31.42.5', 'wustl_inst', 'wustl_pass', 'mod3_g'); 
            if(!isset($_SESSION['user_id'])){
                header("Location: mainpage.php");
            }

    		$body = htmlentities($_POST['body']);
                $user = htmlentities($_SESSION['user_id']);
                $story_id = htmlentities($_POST['story_id']);

            
            $stmt = $mysqli->prepare("INSERT INTO comments (story, user, content) VALUES (?, ?, ?)");
                if(!$stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
            $stmt->bind_param('sss', $story_id, $user, $body);
            $stmt->execute();
            $stmt->close();


            header("Location: homepage.php");
    	?>

    </div>

</body>

</html>