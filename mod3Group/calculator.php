<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>

<link href='http://fonts.googleapis.com/css?family=Indie+Flower'
rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Gloria+Hallelujah' rel='stylesheet' type='text/css'>

	<title>Calculator</title>


<style type="text/css">

p.heading {
font-family: 'Indie Flower', cursive;
font-size: 40px;
text-shadow: 4px 4px 4px #aaa;
text-align: center;
}

body{
	width: 760px; /* how wide to make your web page */
	background-color: #81DAF5; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
        font-family: 'Gloria Hallelujah', cursive;
	font-size: 25px;
        text-align: center;
        color:#F1FAC0;
}


label{
font-size:15px;
}

p.listname{
font-size:30px;
text-decoration: underline;
}

ul.center{
list-style: none;
text-align:center;
}

ul.center li:before {
  content: "\2022  "
}

div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}

</style>

</head>
<body>

<p class = "heading">Calculator</p>

<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
        <input type="radio" id="addition" name="addition" value="addition"><label>Addition</label><br>
        <input type="radio" id="subtraction" name="subtraction" value="subtraction"><label>Subtraction</label><br>
        <input type="radio" id="multiplication" name="multiplication" value="multiplication"><label>Multiplication</label><br>
        <input type="radio" id="division" name="division" value="division"><label>Division</label><br>

 <div id="Input #1">
    <label for="input1">Input #1:</label>
    <input type="number" id="input1" name="input1">
  </div>
 <div id="Input #2">
    <label for="input2">Input #2:</label>
    <input type="number" id="input2" name="input2">
  </div>
<input type="submit" value="Solve" />
</form>

<?php
if( isset($_POST['input1']) and isset($_POST['input2'])){
	$input1 = htmlentities($_POST['input1']);
	$input2 = htmlentities($_POST['input2']);
	if( isset($_POST['addition'])) {
	   $solution = $input1 + $input2;
	   printf("<p><strong>%s</strong></p>\n",
		htmlentities($solution));
	}
	if( isset($_POST['subtraction'])){
	   $solution = $input1 - $input2;
	   printf("<p><strong>%s</strong></p>\n",
		htmlentities($solution));
	}
	if( isset($_POST['multiplication'])){
	   $solution = $input1 * $input2;
	   printf("<p><strong>%s</strong></p>\n",
		htmlentities($solution));
	}
	if( isset($_POST['division'])){
	   $solution = $input1 / $input2;
	   printf("<p><strong>%s</strong></p>\n",
		htmlentities($solution));
	}	
}
?>

</body>
</html>