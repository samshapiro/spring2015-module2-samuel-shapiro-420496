<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>

<link  href ="bootstrap.min.css" rel= "stylesheet">
<link href="starter.css" rel="stylesheet">	
<title>User Directory</title>
</head>
<body>
	
<?php
session_start();
$dirname = dirname(__FILE__);
$user_filenames = scandir(dirname(__FILE__) . "/" . $_SESSION['current_user']);
//echo $user_filenames[0];
//echo $user_filenames[1];
//echo $user_filenames[2];
$num_files = sizeof($user_filenames) - 2;
$file1 = $dirname . "/" . $_SESSION['current_user'] . "/" . "samshome.php";
?>

<p class = "heading">User Directory:</p>
<p class = "heading"> <h1><?php echo $_SESSION['current_user'] ?></h1> </p>
<form action='logout.php' method = 'post' >
	<button type="submit" class="btn btn-danger"> Logout </button>
</form>

<form class="form-inline" enctype='multipart/form-data' action='upload_file.php' method='post'>
			<p>
			<input type='hidden' name='MAX_FILE_SIZE' value='20000000' />
			<label for='uploadfile_input'>Choose a file to upload:</label>
			<input name='uploadedfile' type='file' id='uploadfile_input'/>
			<button type="submit" class="btn btn-primary"> Upload File </button>
			</p>
</form>
<p class="listname">Files:</p>
<ul class="center">
	<?php
	if($num_files==0) exit;
	$i=0;
	$ii=0;
	while ($i < $num_files) {
		$ii=$i+2;	
		echo "<li> $user_filenames[$ii] </li>";
		printf("<form action='view_file.php' method = 'post'>
		       <input type='hidden' name='filename' value='%s' />
		       <input type='submit' name='View' value='View'/></form>", $user_filenames[$ii]);
		printf("<form action='delete_file.php' method = 'post'>
		       <input type='hidden' name='filename' value='%s' />
		       <input type='submit' name='Delete' value='Delete'/></form>", $user_filenames[$ii]);
		$i=$i+1;
	}?>
	
</ul>

</body>
</html>