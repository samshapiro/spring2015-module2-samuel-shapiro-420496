<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>

<link  href ="bootstrap.min.css" rel= "stylesheet">
<link href="starter.css" rel="stylesheet">

</head>
<body>

<p class = "heading">Login</p>

<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">

 <div id="username">
    <input class="form-control" placeholder="Username" type="text" id="username" name="username">
  </div>
<button type="submit"  class="btn btn-primary btn-lg btn-block" id="Login" name="Login" value="Login"> Login </button>

</form> 

<?php
session_start();
if(isset($_POST['Login']) ){
    $username = htmlentities($_POST['username']);
    $_SESSION['current_user'] = $username;
    $file_handle = fopen("shapiro/users.txt", "r");
    $name_handle = 1;
    $user_exists = false;
    while (!feof($file_handle)) {
       $line = fgets($file_handle);
       $user = 'u' . $name_handle;
       $_SESSION[$user] = $line;
       if (trim($line) == trim($username)) $user_exists = true;
       $name_handle = $name_handle + 1;
    }
    $num_users = $name_handle - 1;
    $_SESSION['num_users'] = $num_users;
    fclose($file_handle);
    if ($user_exists == true) {
        header("Location: ../user_directory.php");
        exit;
    }
    echo 'Error: User does not exist.';
}
?>

</body>
</html>